import os
import subprocess

CRD_SSH_Code = "DISPLAY= /opt/google/chrome-remote-desktop/start-host --code=\"4/0ATx3LY40KZQm_1vI_SBDBgiRf5k1XEhtHXJfbKjwzXCFJttE_ONNHd8MN0VHnBPOduLi-A\" --redirect-url=\"https://remotedesktop.google.com/_/oauthredirect\" --name=$(hostname)"
username = "user"
password = "123456"
Pin = 123456
Autostart = True

def run_command(command):
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    if result.returncode != 0:
        print(f"Command failed: {command}")
        print(f"Error: {result.stderr}")
    else:
        print(result.stdout)

os.system(f"useradd -m {username}")
os.system(f"adduser {username} sudo")
os.system(f"echo '{username}:{password}' | sudo chpasswd")
os.system("sed -i 's/\/bin\/sh/\/bin\/bash/g' /etc/passwd")

class CRDSetup:
    def __init__(self, user):
        run_command("apt update")
        self.installCRD()
        self.installDesktopEnvironment()
        self.installGoogleChrome()
        self.installQbit()
        self.finish(user)

    @staticmethod
    def installCRD():
        run_command('wget https://dl.google.com/linux/direct/chrome-remote-desktop_current_amd64.deb')
        run_command('dpkg --install chrome-remote-desktop_current_amd64.deb')
        run_command('apt install --assume-yes --fix-broken')
        print("Chrome Remote Desktop Installed!")

    @staticmethod
    def installDesktopEnvironment():
        run_command("export DEBIAN_FRONTEND=noninteractive")
        run_command("apt install --assume-yes xfce4 desktop-base xfce4-terminal")
        run_command('bash -c \'echo "exec /etc/X11/Xsession /usr/bin/xfce4-session" > /etc/chrome-remote-desktop-session\'')
        run_command("apt remove --assume-yes gnome-terminal")
        run_command("apt install --assume-yes xscreensaver")
        run_command("sudo service lightdm stop")
        run_command("sudo apt-get install dbus-x11 -y")
        run_command("service dbus start")
        print("XFCE4 Desktop Environment Installed!")

    @staticmethod
    def installGoogleChrome():
        run_command("wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb")
        run_command("dpkg --install google-chrome-stable_current_amd64.deb")
        run_command('apt install --assume-yes --fix-broken')
        print("Google Chrome Installed!")

    @staticmethod
    def installQbit():
        run_command("sudo apt update")
        run_command("sudo apt install -y qbittorrent")
        print("Qbittorrent Installed!")

    def finish(self, user):
        run_command(f"adduser {user} chrome-remote-desktop")
        command = f"{CRD_SSH_Code} --pin={Pin}"
        run_command(f"su - {user} -c '{command}'")
        run_command("service chrome-remote-desktop start")
        if Autostart:
            with open(f"/etc/systemd/system/chrome-remote-desktop@{user}.service", "w") as service_file:
                service_file.write(f"""[Unit]
Description=Chrome Remote Desktop service for {user}
After=network.target

[Service]
Type=simple
User={user}
PAMName=login
ExecStart=/opt/google/chrome-remote-desktop/chrome-remote-desktop --start

[Install]
WantedBy=default.target""")
            run_command("systemctl enable chrome-remote-desktop@{user}")
        print("Setup Complete! Please restart the machine.")

try:
    if CRD_SSH_Code == "":
        print("Please enter authcode from the given link")
    elif len(str(Pin)) < 6:
        print("Enter a pin with 6 or more digits")
    else:
        CRDSetup(username)
except NameError as e:
    print("'username' variable not found, Create a user first")
